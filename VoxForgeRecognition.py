
import time

import sys
from AcoModels import AcoModel
from AcoModels import AcoModelSet

from ArkReader import ArkReader
from Token import TokenPassing
from ConstructGraph import ConstructGraph
from calc_wer import calc_wer

from ASRlesson080.CalcWavDurations import calc_rtf

from multiprocessing import Pool
from functools import partial

process_count = 8


def multu_process_recognition(features_and_filename, phoneme_model_path, dict_path):

    ''' Loading graph '''
    aco_model = AcoModelSet.load_aco_model_set(phoneme_model_path)
    head_node = ConstructGraph.costruct(dict_path, aco_model)

    ''' Create and initialize TokenPassing object '''
    engine = TokenPassing(head_node, 100)

    word, final_tokens = engine.run(features_and_filename[1], 200)

    return features_and_filename[0], word


def recognition(phoneme_model_path, dict_path, to_recognize_data):
    """ """

    ''' Loading graph '''
    # aco_model = AcoModelSet.load_aco_model_set(phoneme_model_path)
    # head_node = ConstructGraph.costruct(dict_path, aco_model)

    ''' Create ark file reader '''
    features_to_recognize = ArkReader()

    ''' Read a file with features'''
    features_to_recognize.read_file(to_recognize_data)

    # ''' Create and initialize TokenPassing object '''
    # engine = TokenPassing(head_node, 100)

    ''' Init vars to calculate an error '''
    patterns = 0
    errors = 0

    # count_of_files = len(features_to_recognize.get_all_file_names())

    ''' Recognition result '''
    res = []

    p = Pool(process_count)
    features_to_test = []

    ''' Start recognizer for all files has read from "to_recognize_data" '''
    for _file in features_to_recognize.get_all_file_names():

        features_to_test.append([_file, features_to_recognize.get_feature(_file)]);

    patterns = len(features_to_test)

    proccess_res = p.map(partial(multu_process_recognition, phoneme_model_path=phoneme_model_path, dict_path=dict_path),
                features_to_test)

    for _task_res in  proccess_res:
        res.append((_task_res[0].split('.')[0], _task_res[1].replace('<sil>', '')))


        # word, final_tokens = engine.run(features_to_recognize.get_feature(_file), 200)
        #
        # etalon = _file.split('.')[0]
        # etalon = '<sil>' + etalon.replace('-', ' <sil>') + ' <sil>'
        #
        # # print(_file, ' to : ', word)
        #
        # patterns += 1
        #
        # if etalon.find(word) == -1:
        #     errors += 1
        #     print('ERROR in ', _file)
        #
        # # print('Pattern {}/{} err : {}'.format(patterns, count_of_files, errors))
        #
        # res.append((_file.split('.')[0], word.replace('<sil>', '')))

    return patterns, errors, res


def run_da_net(phoneme_model_path):

    dict_path = 'data/da_net/da_net.dic'
    to_recognize_data = 'data/da_net/da_net_test.txtftr'

    ''' Start recognition '''
    patterns, err, _ = recognition(phoneme_model_path, dict_path, to_recognize_data)

    print('myDaNet : WER {:.2f}% [{}/{}]'.format(float(err)/patterns * 100.0, err, patterns))


def run_da_net_street(phoneme_model_path):

    dict_path = 'data/da_net/da_net.dic'
    to_recognize_data = 'data/da_net/street_da_net.txtftr'

    ''' Start recognition '''
    patterns, err, _ = recognition(phoneme_model_path, dict_path, to_recognize_data)

    print('myStreetDaNet : WER {:.2f}% [{}/{}]'.format(float(err)/patterns * 100.0, err, patterns))


def run_digits(phoneme_model_path):

    dict_path = 'data/digits/digits.dic'
    to_recognize_data = 'data/digits/digits_test.txtftr'

    ''' Start recognition '''
    patterns, err, _ = recognition(phoneme_model_path, dict_path, to_recognize_data)

    print('myDigits : WER {:.2f}% [{}/{}]'.format(float(err)/patterns * 100.0, err, patterns))


def run_digits_street(phoneme_model_path):
    dict_path = 'data/digits/digits.dic'
    to_recognize_data = 'data/digits/street_digits.txtftr'

    ''' Start recognition '''
    patterns, err, _ = recognition(phoneme_model_path, dict_path, to_recognize_data)

    print('myStreetDigits : WER {:.2f}% [{}/{}]'.format(float(err) / patterns * 100.0, err, patterns))


def run_da_net_seq(phoneme_model_path):

    dict_path = 'data/DaNetSeq/DaNetSil.dic'
    to_recognize_data = 'data/DaNetSeq/DaNetSeq.txtftr'

    print('Run Da Net Seq:')

    start = time.time()
    ''' Start recognition '''
    patterns, err, res = recognition(phoneme_model_path, dict_path, to_recognize_data)
    end = time.time()

    print('DaNetSeq : WER {:.2f}% [{}/{}]'.format(float(err)/patterns * 100.0, err, patterns))

    with open('data/DaNetRecognitionResult.txt', mode='w') as f:
        for _res in res:
            f.write(_res[0] + ' ' + _res[1] + '\n')

    calc_wer('data/DaNetTest.ref', 'data/DaNetRecognitionResult.txt')
    calc_rtf(end - start, '/home/alex/lessons/ASR/decoder/test_data/data/myDaNetSeq/wavs')


def run_one_file_vox(phoneme_model_path):

    dict_path = 'data/OnlyTestWordsNoOOV.dic'
    to_recognize_data = 'data/Vox10.txtftr'

    print('Run Vox:')

    start = time.time()
    ''' Start recognition '''
    patterns, err, res = recognition(phoneme_model_path, dict_path, to_recognize_data)
    end = time.time()

    print('Vox : WER {:.2f}% [{}/{}]'.format(float(err)/patterns * 100.0, err, patterns))

    with open('data/VoxRecognitionResult.txt', mode='w') as f:
        for _res in res:
            f.write(_res[0] + ' ' + _res[1] + '\n')

    calc_wer('data/Test.ref', 'data/VoxRecognitionResult.txt')
    calc_rtf(end - start, '/home/alex/lessons/ASR/decoder/test_data/data/VoxforgeRu/test/wavs10subset')


def run_all_for_model(phoneme_model_path):

    print('\nModel : ', phoneme_model_path)

    run_one_file_vox(phoneme_model_path)

    # run_da_net_seq(phoneme_model_path)

    # run_da_net(phoneme_model_path)
    # run_da_net_street(phoneme_model_path)
    # run_digits(phoneme_model_path)
    # run_digits_street(phoneme_model_path)


if __name__ == '__main__':

    # phoneme_models = ('data/VoxForge/ph_models_MIX_sk_1'
    #                   , 'data/VoxForge/ph_models_MIX_sk_2'
    #                   , 'data/VoxForge/ph_models_MIX_sk_2_4'
    #                   , 'data/VoxForge/ph_models_MIX_sk_2_10'
    #                   , 'data/VoxForge/ph_models_MIX_sk_4'
    #                   , 'data/VoxForge/ph_models_MIX_sk_4_8'
    #                   , 'data/VoxForge/ph_models_MIX_sk_10'
    #                   , 'data/VoxForge/ph_models_MIX_sk_10_20'
    #                   , 'data/VoxForge/ph_models_MIX_sk_20')

    phoneme_models = ('data/VoxForge/ph_models_MIX_sk_2_10', )

    for _model in phoneme_models:
        run_all_for_model(_model)

