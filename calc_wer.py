import subprocess

import sys

KALDI_WER_PATH = '/home/alex/projects/ASR/kaldi/src/bin/compute-wer'


def calc_wer(train_file, res_file):
    proc = subprocess.Popen([KALDI_WER_PATH,
                             '--text',
                             '--mode=present',
                             'ark:' + train_file,
                             'ark:' + res_file],
                             stdout=sys.stdout,
                             stderr=sys.stderr
                            )

    proc.wait()


if __name__ == '__main__':

    calc_wer('data/Test.ref', 'data/RecognitionResult.txt')